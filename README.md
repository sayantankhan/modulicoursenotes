# ModuliCourseNotes

Notes for *Moduli of Curves* taught by Aaron Pixton at the University of Michigan in the Winter 2020 term.
This [link](https://gitlab.com/sayantankhan/modulicoursenotes/-/jobs/artifacts/master/browse?job=compile_pdf)
has the pdf generated by the `tex` files in this repository.
